import { EntityService } from "./entity.service";
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';


describe('EntityService', () => {
let entityService: EntityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EntityService,
        {
          provide: HttpClient,
          useValue: {
            get: (url, options) => {
              return of({
                msg: 'Entities found',
                payload: [
                  {
                    value: "shocks",
                  },
                  {
                    value: "suspension"
                  }
                ]
              })
            }
          }
        }
      ],
    });
    entityService = TestBed.inject(EntityService);
  });

  afterEach(() => {
    TestBed.resetTestingModule();
  })
    
  it('getEntities should call entities endpoint', (done) => {
    jest.spyOn(entityService.http, 'get');
    entityService.getEntities('0')
      .subscribe(entities => {
        expect(entityService.http.get).toHaveBeenCalledWith('http://localhost:3000/entity-types/entities/0');
        done();
      })
  });
});