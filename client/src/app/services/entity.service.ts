import { Injectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class EntityService {

  entitiesUrl = environment.entitiesUrl;

  constructor(public http: HttpClient) {
  }

  /***
   * Get entities for entityTypeID
   * @param entityTypeId the ID of what entities you are requesting.
   */
  getEntities(entityTypeId: string) {
    return this.http.get<any>(`${this.entitiesUrl}/${entityTypeId}`)  
                .pipe(map(result => result.payload));
  }
}
