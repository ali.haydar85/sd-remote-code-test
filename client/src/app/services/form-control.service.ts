import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { FormItemBase } from './form-item-base';

@Injectable()
export class FormControlService {
  constructor() { }

  toFormGroup(formItems: FormItemBase<string>[] ) {
    let group: any = {};

    formItems.forEach(formItem => {
      group[formItem.key] = formItem.required ? new FormControl(formItem.value || '', Validators.required)
                                              : new FormControl(formItem.value || '');
    });
    return new FormGroup(group);
  }
}
