import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { FormItemBase } from '../services/form-item-base';
import { Router } from '@angular/router';
import { EntityService } from '../services/entity.service';
import { FormTypeService } from '../services/form-type.service';

@Component({
  selector: 'app-home-page',
  template: `
    <div style="display: flex; flex-direction: column;">
      <h2>Create form </h2>
      <button id="IssueRecordButton" style="margin-right: auto; margin-bottom:8px" (click)="openForm('Issue Record')">Issue record</button>
      <button id="MaintenanceLogButton" style="margin-right: auto; margin-bottom:8px" (click)="openForm('Maintenance Log')">Maintenance log</button>
    </div>
  `
})
export class HomePageComponent {
  loading = false;

  constructor(
    public router: Router,
  ) {
  }

  async openForm(formName: string) {
    this.router.navigate(['form', { formName }]);
  }

}