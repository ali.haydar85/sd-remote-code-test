import { Component, Input } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { FormItemBase }     from '../services/form-item-base';

@Component({
  selector: 'app-form-item',
  templateUrl: './dynamic-form-item.component.html'
})
export class DynamicFormItemComponent {
  @Input() formItem: FormItemBase<string>;
  @Input() form: FormGroup;
}
