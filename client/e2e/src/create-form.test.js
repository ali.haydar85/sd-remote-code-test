Feature('Create a form');

Scenario(' called issue log', async (I) => {
    I.amOnPage('/');
    // check for homepage content.
    I.waitForText('Create form');
    I.seeTextEquals('Create form', 'h2');
    I.seeTextEquals('Issue record', '#IssueRecordButton');
    I.seeTextEquals('Maintenance log', '#MaintenanceLogButton');

    // move to create issue record page
    I.click('#IssueRecordButton');
    I.waitForInvisible('ngx-loading > div', 5);

    // Fill in issue page
    I.selectOption('Component', 'Shocks');
    I.seeInField('Component', 'Shocks');

    I.fillField('Issue', 'There is something shocking about these shocks. It needs looking into.');
    I.seeInField('Issue', 'There is something shocking about these shocks. It needs looking into.');

    I.click('Save');
    I.amAcceptingPopups();
    I.seeInPopup('Form saved!!');
});
