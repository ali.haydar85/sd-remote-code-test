# TLS
## How to create a self-signed certificate with OpenSSL
> openssl req -x509 -newkey rsa:4096 -keyout dev-private-key.pem -out dev-public-cert.pem -days 365 -nodes -subj '/CN=localhost'